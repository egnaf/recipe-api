package com.example.recipe.configurations;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MappingConfiguration {

    @Bean
    public DozerBeanMapper getMapper() {
        return new DozerBeanMapper();
    }
}
