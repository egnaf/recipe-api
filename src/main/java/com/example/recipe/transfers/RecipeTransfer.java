package com.example.recipe.transfers;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RecipeTransfer {

    private long id;
    private String name;
    List<IngredientTransfer> ingredients = new ArrayList<>();
}
