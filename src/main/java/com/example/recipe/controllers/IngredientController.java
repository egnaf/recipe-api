package com.example.recipe.controllers;

import com.example.recipe.models.Ingredient;
import com.example.recipe.services.IngredientService;
import com.example.recipe.transfers.IngredientTransfer;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ingredients")
public class IngredientController {

    private final IngredientService ingredientService;
    private final Mapper mapper;

    @Autowired
    public IngredientController(IngredientService ingredientService, Mapper mapper) {
        this.ingredientService = ingredientService;
        this.mapper = mapper;
    }

    @PostMapping("/add")
    public IngredientTransfer add(@RequestBody IngredientTransfer ingredient) {
        return mapper.map(ingredientService.add(mapper.map(ingredient, Ingredient.class)), IngredientTransfer.class);
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(ingredientService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable long id) {
        return ResponseEntity.ok(ingredientService.getById(id));
    }
}
