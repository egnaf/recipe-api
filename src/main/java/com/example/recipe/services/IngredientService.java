package com.example.recipe.services;

import com.example.recipe.models.Ingredient;

import java.util.List;

public interface IngredientService {

    Ingredient add(Ingredient ingredient);

    List<Ingredient> getAll();

    Ingredient getById(long id);

    List<Ingredient> getAllByRecipeId(long id);
}
